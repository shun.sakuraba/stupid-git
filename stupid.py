#!/usr/bin/env python

# Copyright (c) 2014 Shun Sakuraba
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""STUPID translator - translates Fortran codes into explicitly SIMDized Fortran codes, which should have been done by smart compilers

STUPID translator translates Fortran programs with directives into another Fortran program.
I wrote this program because some - if not all - compilers are very annoyingly stupid, giving up SIMDizing too early, even if there are enough directives and compiler options. The reason of giving up ranges from, for example, if there are indirect accesses to the table, or the reduction computations are required. This script rewrites annotated scripts into loops.

Example of supported directives:

!STUPID CONSTUNROLL(1, 2, 3)
!STUPID UNROLL REDUCTION(+:energy, r)
!STUPID SERIAL
!STUPID DECLARE BEGIN
!STUPID DECLARE END
!STUPID OMP
!STUPID VECONLY(!FOO BAR)

Note: this script is full of ad-hoc hacks.
"""



import os
import sys
import re
# To support Python 2.3. For ancient CentOS5.
from optparse import OptionParser

parser = OptionParser()

parser.add_option("-f", "--input", dest = "input", action = "store", type = "str",
                  help = "Source file",
                  default = "-")

parser.add_option("-o", "--output", dest = "output", action = "store", type = "str",
                  help = "Output file",
                  default = "-")


parser.add_option("--indent", dest = "indent", action = "store", type = "int",
                  help = "Indent depth",
                  default = 3)

parser.add_option("--unroll", dest = "unroll", action = "store", type = "str",
                  help = "Unroll width; can be variable",
                  default = "4")

parser.add_option("--indent-veconly", dest = "indent_veconly", action = "store_true",
                  help = "Force VECONLY operator output indented pragma (Note Fujitsu frt does not understand non-first column pragma)",
                  default = False)

parser.add_option("-v", "--verbose", dest = "verbose", action = "store_true",
                  help = "Increase verbosity (for debug)",
                  default = False)

(options, args) = parser.parse_args()

def log(x):
    if options.verbose:
        print >> sys.stderr, x


def warn_or_error(x, col):
    prefix = ""
    suffix = ""
    if os.isatty(sys.stderr.fileno()):
        prefix = "\x1b[" + str(col) + ";1m"
        suffix = "\x1b[0m"
    print >> sys.stderr, prefix + x + suffix

def warn(x):
    warn_or_error("Warning: " + x, 33)

def error(x):
    warn_or_error("Error: " + x, 31)
    sys.exit(1)

def syntax_error(line, msg, guess = None):
    if 'line_convert' in globals():
        line = line_convert[line]
    errmsg = "line %d: %s" % (line + 1, msg)
    if guess:
        errmsg += "\n (error occured around \"%s\")" % guess
    error(errmsg)

# This class is used as a mere structure.
class directive_info:
    def __init__(self):
        self.dirline = None
        self.do_range = None
        self.declare_begin = None
        self.declare_end = None
        self.omp = None
        self.reduc_vars = []
        self.reduc_types = {}
        self.functions = []
        self.type_dic = {}
        self.dim_dic = {}
        self.loop_info = {}
        self.generated = None

    def dump(self):
        """dump directive info for debugging purpose"""

        print self.dirline
        print self.do_range
        print self.declare_begin
        print self.declare_end
        print self.omp
        print self.omp_end
        print self.reduc_vars
        print self.reduc_types
        print self.functions
        print self.type_dic
        print self.dim_dic
        print self.loop_info
        print self.generated

    def typeof(self, var):
        return self.type_dic[var]

# Global variables to be used. 
if options.input == '-':
    lines = sys.stdin.readlines()
else:
    fh = open(options.input, "rt")
    lines = fh.readlines()
    fh.close()
parsed = []
addenda = {}
unrolled_lines = []
directives = {}

# Utility function
def find_do(lno):
    dirlno = lno
    do_level = 0
    do_begin = None
    while True:
        if lno >= len(lines):
            error("line %d: ENDDO or END DO statement could not be found" % dirlno)
        l = lines[lno].upper().lstrip()
        ls = l.split()
        if ls[0:1] == ["DO"]:
            do_level += 1
            if do_level == 1:
                do_begin = lno
        elif ls[0:1] == ["ENDDO"] or ls[0:2] == ["END", "DO"]:
            do_level -= 1
            if do_level == 0:
                if do_begin == None:
                    error("line %d: No DO statement before line %d" % (dirlno, lno))
                return (do_begin, lno)
        lno += 1

def parse_do(l):
    m = re.match(r"(?P<indent>\s*)do\s+(?P<var>[^=]+)=\s*(?P<loopbegin>[^,]+),\s*(?P<loopend>[^,]+)$",
                     l,
                     re.IGNORECASE)
    if m:
        return {
            "indent": m.group("indent"),
            "var": m.group("var").strip(),
            "loopbegin": m.group("loopbegin").strip(),
            "loopend": m.group("loopend").strip()
            }

# Parse lines to find constant unroll directives
line_convert = {}
constant_unrolls = {}

for lno in range(len(lines)):
    l = lines[lno]
    ltrunk = l.lstrip()
    m = re.match(r"(?P<indent>\s*)!STUPID\s+CONSTUNROLL\s*\((?P<vals>(?:\w+)(?:\s*,\s*\w+\s*)*)\)\s*$", l)
    if m:
        unroll_values = m.group("vals").split(",")
        unroll_values = [u.strip() for u in unroll_values]
        indent = m.group("indent")
        (do_begin, do_end) = find_do(lno + 1)
        doinfo = parse_do(lines[do_begin])
        if doinfo == None:
            error("Unexpected happened: doinfo after CONSTUNROLL is empty")
        constant_unrolls[lno] = (unroll_values, indent, do_begin, do_end, doinfo)

new_lines = []
lno = 0
while True:
    if lno >= len(lines):
        break
    if lno in constant_unrolls:
        (unroll_values, indent, do_begin, do_end, doinfo) = constant_unrolls[lno]
        for v in unroll_values:
            line_convert[len(new_lines)] = lno
            new_lines.append("%s%s = %s" % (indent, doinfo["var"], v))
            for lno_inner in range(do_begin + 1, do_end):
                line_convert[len(new_lines)] = lno_inner
                new_lines.append(lines[lno_inner])
        lno = do_end + 1
    else:
        line_convert[len(new_lines)] = lno
        new_lines.append(lines[lno])
        lno += 1

lines = new_lines

# parse lines to find directives

declare_lno = None
omp_lno = None

declare_begin = None
for lno in range(len(lines)):
    l = lines[lno]
    ltrunk = l.lstrip()
    if ltrunk[0:8].upper() == "!STUPID ":
        dirs = ltrunk[8:].upper().split()
        if dirs[0:2] == ["DECLARE", "BEGIN"]:
            declare_begin = lno
            declare_end = None
            omp_lno = None
        else:
            if declare_begin == None:
                error('"!STUPID %s" statement should not come before "!STUPID DECLARE BEGIN" at line %d' % (dirs[0], lno))
            if dirs[0:1] == ["OMP"]:
                omp_lno = lno
            elif dirs[0:2] == ["DECLARE", "END"]:
                declare_end = lno
            elif dirs[0:1] == ["UNROLL"]:
                d = directive_info()
                d.dirline = lno
                d.declare_begin = declare_begin
                d.declare_end = declare_end
                d.omp = omp_lno
                directives[lno] = d
            elif dirs[0:1] == ["SERIAL"]:
                # do nothing here
                pass
            else:
                pass
        parsed.append(("dir", dirs))
    else:
        parsed.append(("src", l))

# determine the beginning and the end of unrolling
for dirlno in directives:
    lno = dirlno + 1
    directives[dirlno].do_range = find_do(lno)

unroll_opts_pat = re.compile(r"\s*(?P<ident>[a-zA-Z][a-zA-Z0-9_]*)\s*\((?P<inparen>[^)]+)\)")
# parse unroll directive
for dirlno in directives:
    l = lines[dirlno].lstrip()
    m = re.match("!STUPID\s+UNROLL\s+", l, re.IGNORECASE)
    assert(m != None)
    strbuf = l
    bufpos = m.end(0)
    while True:
        remain = strbuf[bufpos:]
        m = unroll_opts_pat.match(remain)
        if m == None:
            # is there any remaining token?
            if remain.strip() != "":
                syntax_error(dirlno, "lexer error", remain.strip())
            else:
                # No more token to process
                break
        matchresult = m.groupdict()
        ident = matchresult["ident"]
        inparen = matchresult.get("inparen")

        # parse unroll directive
        if ident.upper() == "REDUCTION":
            if inparen == None:
                syntax_error(dirlno, "UNROLL REDUCTION requires clause inside \"()\"")
            reductions = inparen.split(",")
            reductions = [s.strip() for s in reductions]
            operator = None
            for r in reductions:
                varname = r
                if r.find(":") == -1:
                    if operator == None:
                        syntax_error(dirlno, "reduction operator for variable \"%s\" is not declared" % r)
                else:
                    rs = r.split(":")
                    if len(rs) != 2:
                        syntax_error(dirlno, "parse error", r)
                    operator = rs[0].strip()
                    varname = rs[1].strip()
                directives[dirlno].reduc_vars.append(varname)
                directives[dirlno].reduc_types[varname] = operator
        elif ident.upper() == "FUNCTION":
            warn("UNROLL FUNCTION is currently unused")
            if inparen == None:
                syntax_error(dirlno, "UNROLL FUNCTION requires clause inside \"()\"")
            functions = inparen.split(",")
            functions = [s.strip() for s in functions]
            directives[dirlno].functions += functions
        else:
            syntax_error(dirlno, "Unknown UNROLL directive: \"%s\"" % ident)
        bufpos += m.end(0)

    # parse next statement to find "DO" loop
    dolno = None
    for lno in range(dirlno + 1, len(lines)):
        l = lines[lno]
        if l[0] == 'c':
            continue
        ls = l.split("!")
        if ls == []:
            continue
        l = ls[0]
        if l.strip() == "":
            continue

        doinfo = parse_do(l)
        if doinfo:
            dolno = lno
            loop_info = {
                "indent": doinfo["indent"],
                "do_lno": dolno,
                "varname": doinfo["var"],
                "loopbegin": doinfo["loopbegin"],
                "loopend": doinfo["loopend"]
                }
            break
        else:
            syntax_error(dirlno, "Expected do loop after UNROLL directive")

    if dolno == None:
        syntax_error(dirlno, "Could not find \"do\" statement after the unroll directive")

    depth = 1
    enddolno = None
    for lno in range(dolno + 1, len(lines)):
        l = lines[lno]
        if re.match(r"\s*do\s+", l, re.IGNORECASE):
            depth += 1
            continue
        if re.match(r"\s*end ?do(\s+|$)", l, re.IGNORECASE):
            depth -= 1
            if depth == 0:
                enddolno = lno
                break;

    if enddolno == None:
        syntax_error(dirlno, "Could not find \"end do\" / \"enddo\" statement after the unroll directive")

    loop_info["enddo_lno"] = enddolno

    # Scan lines to find !STUPID SERIAL
    pat = re.compile(r"\s*!STUPID\s+SERIAL\s*$", re.IGNORECASE)
    for lno in range(dolno + 1, enddolno):
        if pat.match(lines[lno]):
            loop_info["serial"] = lno
            break
    
    # Update loop information
    directives[dirlno].loop_info = loop_info

    # Parse variable declarations
    # TODO: complex
    pats = {
        "real(4)": r"\s*real\s*(?:\(\s*4\s*\))?\s*(?!\()",
        "real(8)": r"\s*(?:real\s*\(\s*8\s*\)|double\s*precision)\s*",
        "integer": r"\s*integer\s*(?:\(\s*4\s*\))?\s*(?!\()",
        "integer(8)": r"\s*integer\s*\(\s*8\s*\)\s*",
        "logical": r"\s*logical\s*(?:\(\s*[1248]\s*\))?\s*"
        }
    # for loop one-liner in dictionary is not allowed with 2.6
    patscompiled = {}
    for k in pats:
        patscompiled[k] = re.compile(pats[k], re.IGNORECASE)
    dr = directives[dirlno]
    for lno in range(dr.declare_begin + 1, dr.declare_end):
        l = lines[lno]
        if l[0] == 'c':
            continue
        l = l.split("!")[0]
        if l.strip() == "":
            continue
        typefound = None
        typeend = -1
        for vtype in patscompiled:
            vp = patscompiled[vtype]
            m = vp.match(l)
            if m != None:
                typefound = vtype
                typeend = m.end(0)
                break
        if typefound == None:
            warn("Unknown type specification at line " + str(lno + 1))

        # determine starting position
        pos = typeend
        if l.find("::") != -1:
            pos = l.find("::") + 2
        
        varpat = re.compile("\s*(?P<ident>[A-Za-z][A-Za-z0-9_]*)\s*(?P<dim>\([^)]*\))?\s*(?:,?)\s*(=\s*[^,\s]+\s*)?")
        while True:
            m = varpat.match(l, pos)
            if m == None:
                if pos != len(l):
                    warn("Parsing aborted prematurely (line %d, \"%s\")" % (lno, l[pos:]))
                break
            pos = m.end(0)
            directives[dirlno].type_dic[m.group("ident")] = typefound
            if "dim" in m.groupdict() and m.groupdict()["dim"] != None:
                directives[dirlno].dim_dic[m.group("ident")] = m.group("dim")

    # find omp directive lines following omp line
    if dr.omp:
      pat = re.compile(r"\s*[!c]\$OMP(\s*)", re.IGNORECASE)
      lno = dr.omp + 1
      if pat.match(lines[lno]) == None:
          syntax_error(dr.omp, "STUPID OMP must be followed by !$OMP statements")
      lno += 1
      while True:
          if lno >= len(lines):
              syntax_error(dr.omp, "unexpected end of file after STUPID OMP")
          if pat.match(lines[lno]) == None:
              directives[dirlno].omp_end = lno - 1
              break
          else:
              lno += 1

veconly_pat = re.compile(r"(\s*)!STUPID\s+VECONLY\s*\((.*)\)\s*$", re.IGNORECASE)

fresh_v = 0

# Start generating unrolled statements
for dirlno in sorted(directives):
    dr = directives[dirlno]
    # dr.dump()

    def fresh():
        # return fresh identifier
        global fresh_v
        while True:
            varname = "i_%d" % fresh_v
            fresh_v += 1
            if varname not in dr.type_dic:
                return varname

    def init_of_var(v):
        reduc_var = v;
        reduc_type = dr.reduc_types[reduc_var]
        return {
            ("real(8)", "+"): "0.0d0",
            ("real(4)", "+"): "0.0f0",
            ("integer(8)", "+") : "0",
            ("integer", "+") : "0"
            }[(dr.typeof(v), reduc_type)]

    def reduc_of_var(v):
        reduc_var = v;
        reduc_type = dr.reduc_types[reduc_var]
        return {
            ("+"): (lambda x: "sum(%s, 1)" % x)
            }[reduc_type]

    def vec_var_of(v):
        return v + "_vec"

    generated = []
    li = dr.loop_info
    baseindent = li["indent"]
    vectorized = []
    loop_subvar = fresh()
    loopvars = [loop_subvar]

    # Prologue - initialize reduction variables
    for r in dr.reduc_vars:
        generated.append("%s%s = %s" % (baseindent, vec_var_of(r), init_of_var(r)))
        vectorized.append(r)

    # Prologue - output comment lines if necessary
    for lno in range(dirlno + 1, dr.do_range[0]):
        m = veconly_pat.match(lines[lno])
        if m:
            indent = m.group(1)
            pragma = m.group(2)
            if options.indent_veconly:
                generated.append("%s%s" % (indent, pragma))
            else:
                generated.append("%s" % pragma)
        else:
            generated.append(lines[lno])

    # Start vectorized loop
    generated.append("%(indent)sdo %(loopvar)s = %(loopbegin)s, %(loopend)s - mod(%(loopend)s - %(loopbegin)s + 1, %(veclen)s), %(veclen)s" %
                     { "indent": baseindent,
                       "loopvar": li["varname"],
                       "loopbegin": li["loopbegin"],
                       "loopend": li["loopend"],
                       "veclen": options.unroll })

    identpat = r"[a-z][a-z0-9_]*"
    # Replace statements to vectorized calculation

    do_lno = li["do_lno"]
    enddo_lno = li["enddo_lno"]
    has_serial_section = li.has_key("serial")
    serialized = False
    serialize_indent = None
    for lno in range(do_lno + 1, enddo_lno):
        l = lines[lno]

        if has_serial_section and li["serial"] == lno:
            m = re.match(r"(\s*)!STUPID\s+SERIAL\s*$", l, re.IGNORECASE)
            assert(m)
            serialize_indent = m.group(1)
            generated.append("%sdo %s = 1, %s" % (serialize_indent, loop_subvar, options.unroll))
            serialized = True
            continue
            
        # is this a computation statement?
        # This pattern is quite an ad-hoc. [^=] is used in leftvec pattern to allow
        # x(f(i)) = ...
        # and disallow
        # if(x == 3) cycle
        m = re.match(r"(?P<indent>\s*)(?P<leftvar>%(ident)s)\s*(?P<leftvec>\([^=]*\))?\s*=" % {"ident": identpat }, 
                     l,
                     re.IGNORECASE)
        if m:
            indent = m.group("indent")
            leftvar = m.group("leftvar")
            leftvec = m.groupdict().get("leftvec")
        else:
            m = veconly_pat.match(l)
            if m:
                indent = m.group(1)
                if serialized and indent != "":
                    indent += " " * options.indent
                pragma = m.group(2)
                if options.indent_veconly:
                    generated.append("%s%s" % (indent, pragma))
                else:
                    generated.append("%s" % pragma)
                continue
            if not serialized:
                generated.append(l)
                continue
            m = re.match(r"(\s*)(?P<skip>(!|$))?", l)
            if "skip" in m.groupdict() and m.group("skip") != None:
                # blank line
                assert(serialized)
                generated.append(m.group(1) + " " * options.indent + l[m.end(1):])
                continue
            indent = m.group(1)
            lefvar = None
            leftvec = None

        # find all occurence of the identifier-like-tokens
        if leftvec == None and leftvar != None and not serialized:
            vectorized.append(leftvar)
        pos = 0
        identpos = []
        # prevent "1.0d0" being understood as "1.0" "d" "0"
        # FIXME TODO: in-line comment
        identpat_nonumber = r"(?<![0-9.])%s" % identpat
        pat = re.compile(identpat_nonumber, re.IGNORECASE)

        while True:
            m = pat.search(l[pos:])
            if m == None:
                break
            identpos.append((pos + m.start(0), pos + m.end(0)))
            pos += m.end(0)

        # TODO: current expansion can be replaced by vector notation,
        # supposed that there are no more than one-dimensional vector access.
        if not serialized:
            generated.append("%(indent)sdo %(var)s = 1, %(veclen)s" % 
                             { "indent": indent,
                               "var": loop_subvar,
                               "veclen": options.unroll })
        rec = indent + " " * options.indent
        pos = len(indent)
        parenpat = re.compile(r"\s*\(")
        for (s, e) in identpos:
            rec += l[pos:s]
            token = l[s:e]
            if token == li["varname"]:
                rec += "%s + %s - 1" % (li["varname"], loop_subvar)
                # rec += ("%(loopvar)s:(%(loopvar)s+%(looplen)s-1)" %
                #         { "loopvar": li["varname"],
                #           "looplen": options.unroll })
            elif token in vectorized:
                # access to originally vector variable is vectorized as well
                m = parenpat.match(l, e)
                if m:
                    rec += "%s(%s, " % (vec_var_of(token), loop_subvar)
                    pos = m.end(0)
                    continue
                rec += "%s(%s)" % (vec_var_of(token), loop_subvar)
            else:
                rec += token
            pos = e
        rec += l[identpos[-1][1]:]
        generated.append(rec)
        if not serialized:
            generated.append("%send do" % indent)

    if serialized:
        generated.append(serialize_indent + "end do")
    generated.append(baseindent + "end do")

    # Epilogue - reduce reduction variables
    for r in dr.reduc_vars:
        generated.append("%s%s = %s + %s" % (baseindent, r, r, (reduc_of_var(r))(vec_var_of(r))))

    # pragma output for remaining loop
    for lno in range(dirlno + 1, dr.do_range[0]):
        if veconly_pat.match(lines[lno]):
            continue
        generated.append(lines[lno])

    # remaining loop - just copy most of them
    generated.append("%(indent)sdo %(loopvar)s = %(loopend)s - mod(%(loopend)s - %(loopbegin)s + 1, %(veclen)s) + 1, %(loopend)s" %
                     { "indent": baseindent,
                       "loopvar": li["varname"],
                       "loopbegin": li["loopbegin"],
                       "loopend": li["loopend"],
                       "veclen": options.unroll })
    for lno in range(dr.do_range[0] + 1, dr.do_range[1]):
        generated.append(lines[lno])
    generated.append(baseindent + "end do")

    for lno in range(dirlno + 1, dr.do_range[1] + 1):
        unrolled_lines.append(lno)
    directives[dirlno].generated = generated
    
    # Determine variable types and array size to be declared
    if dr.omp:
        ompaddenda = addenda.get(dr.omp_end, {})
        for v in vectorized:
            ompaddenda[("omp", vec_var_of(v))] = None
        for v in loopvars:
            ompaddenda[("omp", v)] = ("integer", None)
        addenda[dr.omp_end] = ompaddenda

    if dr.declare_begin:
        decladdenda = addenda.get(dr.declare_end, {})
        for v in vectorized:
            decladdenda[("declare_vec", vec_var_of(v))] = (dr.type_dic[v], dr.dim_dic.get(v))
        addenda[dr.declare_end] = decladdenda

    decladdenda = addenda.get(dr.declare_end, {})
    for v in loopvars:
        decladdenda[("declare", v)] = ("integer", None)
    addenda[dr.declare_end] = decladdenda


def format_omp_variables(fh, el, indent, maxcolumn):
    toplevel_indent = indent[0]
    omp_indent = indent[1]
    indent_w = 0
    for ch in toplevel_indent:
        if ch == " ":
            indent_w += 1
        elif ch == "\t":
            indent_w += 8 # safe upper bound
    remain = maxcolumn - indent_w
    if remain < 15:
        warn("format_omp_variables: indent depth too deep")
        remain = 15
    remain -= len("!$OMPPRIVATE() &")
    txt = ""
    lineremain = remain
    for i in range(len(el)):
        e = el[i][0]
        if txt == "":
            # force add
            lineremain -= len(e)
            txt = e
        elif len(e) + 1 <= lineremain:
            lineremain -= len(e) + 1
            txt += ","
            txt += e
        else:
            fh.write("%s!$OMP%sPRIVATE(%s) &\n" % (toplevel_indent, omp_indent, txt))
            txt = e
            lineremain = remain - len(e)
    fh.write("%s!$OMP%sPRIVATE(%s)\n" % (toplevel_indent, omp_indent, txt))

def format_declare_variables(fh, el, indent, is_vec):
    for (v, (t, d)) in el:
        if d:
            m = re.match(r"\s*\((.*)\)\s*", d)
            if m == None:
                error("Unexpected declare dimension: \"%s\"" % d)
            if is_vec:
                fh.write("%s%s :: %s(%s,%s)\n" % (indent, t, v, options.unroll, m.group(1)))
            else:
                fh.write("%s%s :: %s(%s)\n" % (indent, t, v, m.group(1)))
        else:
            if is_vec:
                fh.write("%s%s :: %s(%s)\n" % (indent, t, v, options.unroll))
            else:
                fh.write("%s%s :: %s\n" % (indent, t, v))

def output(fh):
    fh.write("""!!!! STUPID translator generated this source code by:
!!!! %s %s
!!!! DO NOT MODIFY THIS FILE unless you know what you are doing.
""" % (sys.argv[0], " ".join(["'%s'" % s for s in sys.argv[1:]])))
    for lno in range(len(lines)):
        if lno in addenda:
            addenda_l = addenda[lno]
            # combine for each kind
            addenda_bykind = {}
            for (kind, v) in addenda_l:
                if kind not in addenda_bykind:
                    addenda_bykind[kind] = []
                addenda_bykind[kind].append((v, addenda_l[(kind, v)]))
            for kind in addenda_bykind:
                el = addenda_bykind[kind]
                el.sort()
                if kind == "omp":
                    fh.write(lines[lno].rstrip())
                    fh.write(" &\n")
                    indent = re.match(r"(\s*)[!c]\$OMP(\s*)", lines[lno]).groups()
                    format_omp_variables(fh, el, indent, 80)
                elif kind == "declare_vec":
                    indent = re.match(r"\s*", lines[lno]).group(0)
                    format_declare_variables(fh, el, indent, True)
                elif kind == "declare":
                    indent = re.match(r"\s*", lines[lno]).group(0)
                    format_declare_variables(fh, el, indent, False)
        elif lno in unrolled_lines:
            # print nothing
            pass
        elif lno in directives:
            dr = directives[lno]
            for l in dr.generated:
                if l[-1] != '\n':
                    l += '\n'
                fh.write(l)
        else:
            l = lines[lno]
            if l[-1] != '\n':
                l += '\n'
            fh.write(l)

if options.output == '-':
    output(sys.stdout)
else:
    # In python 2.3 there is no "with" statement. Sigh
    ofh = open(options.output, "wt")
    output(ofh)
    ofh.close()






        
    
