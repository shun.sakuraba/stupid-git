# README #

STUPID translator translates annotated Fortran 90 source codes into hand-vectorized routine.

To squeeze maximum FLOPs from recent processors it is vital to have SIMD-ized code at its heaviest loop. Unfortunately most current compilers give up SIMD-izing loops which have gather / scatter operation, simply because in their heuristics it's not worth it. STUPID translates annotated codes into code with explicit vector operations which force compilers to emit SIMD-ized code.

If you are interested in, try:


```
#!shell

python stupid.py < sample.F90


```

and compare output with example.F90.